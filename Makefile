
CC=g++
CFLAGS=-Wno-write-strings
TARGET0=main.cpp
SOURCES=tiff_ops.cpp png_ops.cpp main.cpp filter.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=homework

all:	$(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ -lpng -lm -g
clean:
	rm -rf *.o *.png
	rm homework
.cpp.o:
	$(CC) $(CFLAGS) -c  $< -o $@ -g


