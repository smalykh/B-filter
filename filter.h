#include <stdio.h>
#include <math.h>
#include <stdlib.h>
	
const double sigmaI 	= 12.0;
const double sigmaS 	= 16.0;
const int  diameter 	= 5;


int makeTransformation(unsigned char* src, unsigned char **filteredImage, int length, int width ) ;
