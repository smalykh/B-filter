#include "filter.h"
#include "png_ops.h"
#include "tiff_ops.h"


int main()
{
	unsigned char *src_image;
	unsigned char *filtered_image;
	int image_length, image_width;

	if (decode_tiff("img.tif", &src_image, &image_length, &image_width)) 		return -1;
	if (makeTransformation(src_image, &filtered_image, image_length, image_width))  return -1;
	if (writeImage ("o.png", image_width, image_length, filtered_image, "xxx"))     return -1;
	return 0;
}
