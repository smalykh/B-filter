#include "filter.h"
	
float distance(int x, int y, int i, int j) 
{
	return float(sqrt(pow(x - i, 2) + pow(y - j, 2)));
}
	
double gaussian(float x, double sigma)
{
	return exp(-(pow(x, 2))/(2 * pow(sigma, 2))) / (2 * 3.14 * pow(sigma, 2));	
}
	
void applyBilateralFilter(unsigned char* source, unsigned char* filteredImage, int length, int width, int x, int y) 
{
	double iFiltered = 0;
	double wP = 0;
	int neighbor_x = 0;
	int neighbor_y = 0;
	int half = diameter / 2;
	
	for(int i = 0; i < diameter; i++) 
	{
		for(int j = 0; j < diameter; j++) 
		{
			neighbor_x = x - (half - i);
			neighbor_y = y - (half - j);
			double gi = gaussian(source[neighbor_x + neighbor_y*width] - source[x + y*width], sigmaI);
			double gs = gaussian(distance(x, y, neighbor_x, neighbor_y), sigmaS);
			double w = gi * gs;
			iFiltered = iFiltered + source[neighbor_x + neighbor_y*width] * w;
			wP = wP + w;
		}
	}
	iFiltered = iFiltered / wP;
	filteredImage[x + y*width] = (unsigned char)iFiltered;
	
	
}
	
unsigned char *bilateralFilter(unsigned char *source, int length, int width) 
{
	unsigned char *filteredImage = (unsigned char*)calloc(length * width, sizeof(unsigned char));
	for(int i = diameter/2; i < length - diameter/2; i++) 
	{
		for(int j = diameter/2; j < width - diameter/2; j++) 
		{
			applyBilateralFilter(source, filteredImage, length, width, j, i);
		}
	}
	return filteredImage;
}
	
	
int makeTransformation(unsigned char* src, unsigned char **filteredImage, int length, int width ) 
{
	if ( !src )
	{
		printf("No image data \n");
		return -1;
	}
	printf("Try to apply filter..\n");
	*filteredImage = bilateralFilter(src, length, width);
	printf("Done!\n");
	return 0;
}
