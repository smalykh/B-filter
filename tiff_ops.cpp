#include "tiff_ops.h"

static bool LSBF;
static unsigned char * buff;

// POSIX-compatible
int get_size_of_file(FILE* pFile)
{
	struct stat st;
	fstat(fileno(pFile), &st);
	return st.st_size;
}

// LSBF or MSBF?
int byte_order_check()
{
	unsigned int start = 0;
	for(int i = 0; i < 4; i++) start |= buff[i] << (3-i)*8;
	printf("Start seq is %x\n", start);
	if(start == MSB_SEQ) LSBF = true;
	else if(start == LSB_SEQ) LSBF = false;
	else return -1;
	printf("LSBF = %d\n", LSBF);
	return 0;
}

// Read short val from data array (LSBF or MSBF)
unsigned short get_short(int index)
{
	return  (!LSBF)? (buff[index] << 8) | buff[index+1] :
				 (buff[index+1] << 8) | buff[index];
}

// Read short val from data array (LSBF or MSBF)
unsigned int get_int(int index)
{
	int t = 0;
	if(!LSBF)
	  for(int i = 0; i < 4; i++) 
		t |= ((unsigned int)(buff[i+index])) << (3-i)*8;
	else		
	  for(int i = 0; i < 4; i++) 
		t |= ((unsigned int)(buff[i+index])) << i*8;
	return t;
}

// Find tag in IFD. 
// start index    - start first tag in IFD
// tag    	  - ID of TAG
// amount of data - amount of tags in IFS 
// return value that tag contains
unsigned long find_tag(int start_index, int tag, int amount_of_tags)
{
	int i;
	for(i = 0; i < amount_of_tags; i++)
	{
		if(tag == get_short(start_index + TAG_LEN*i)) break;
	}
	return get_int(start_index + TAG_LEN*i + OFFSET_TO_DATA_TAG);
}


// Function decode 8bpp grayscale tiff 
// and returns array of image data
int decode_tiff(char *filename, unsigned char **img_data, int *length, int *width)
{
	printf("Try to open file..\n");
	FILE* tiff = fopen(filename, "rb");
	if(!tiff)
	{
		fprintf(stderr, "Error opening file!\n");
		return -1;
	}
	else printf("Done!\n");
	
	// Get size of file	
	int len = get_size_of_file(tiff);
	buff = (unsigned char*)malloc(len * sizeof(unsigned char));
	
	// Copy in array
	fread(buff, sizeof(unsigned char), len, tiff);
	fclose(tiff);
	LSBF =0;
	// Try to find magic number in the start of file
	if(byte_order_check()) 
	{ 	
		fprintf(stderr, "It is not tiff!\n");
		return -1;
	}
	// Seek to start of IFD
	int offset = get_int(HEADER_OFFSET);
	int amount_of_tags = get_short(offset);
	
	// Read parameters from structure
	int rows_per_strip    = find_tag(offset + IFD_OFFSET,  ROWS_PER_STRIP,     amount_of_tags);
	int image_length      = find_tag(offset + IFD_OFFSET,  IMAGE_LENGTH,       amount_of_tags);
	int image_width       = find_tag(offset + IFD_OFFSET,  IMAGE_WIDTH,        amount_of_tags);
	int strip_offset      = find_tag(offset + IFD_OFFSET,  STRIP_OFFSET,       amount_of_tags);
	int strip_byte_counts = find_tag(offset + IFD_OFFSET,  STRIP_BYTES_COUNTS, amount_of_tags);
	int compression       = find_tag(offset + IFD_OFFSET,  COMPRESSION,        amount_of_tags);
	int bits_per_sample   = find_tag(offset + IFD_OFFSET,  BITS_PER_SAMPLE,    amount_of_tags);
	
	// Try to find text IFD
	int next = amount_of_tags*TAG_LEN + offset + IFD_OFFSET;
	int next_IFD_offst = get_short(next);
	
	// Print information about parameters
	printf("\nInformation about image:\n");	
	printf(" IFD offset        = %x\n", offset);
	printf(" Image length      = %x\n", image_length);
	printf(" Image width       = %x\n", image_width);
	printf(" Strip offset      = %x\n", strip_offset);
	printf(" Row per strip     = %x\n", rows_per_strip);
	printf(" Strip byte counts = %x\n", strip_byte_counts);
	printf(" Compression       = %x\n", compression);
	printf(" Bits per sample   = %x\n", bits_per_sample);
	printf(" Next IFD offset   = %x\n", next_IFD_offst);

	// Copy image data in result array
	*img_data = (unsigned char*)malloc(strip_byte_counts * sizeof(unsigned char));
	*length   = image_length;
	*width    = image_width;
	memcpy(*img_data, buff+strip_offset, strip_byte_counts);

	// We dont need it now
	free(buff);

	return 0;
}
