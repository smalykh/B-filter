#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#define MSB_SEQ			(long) 0x49492a00
#define LSB_SEQ			(long) 0x4d4d002a

#define ROWS_PER_STRIP		0x0116
#define IMAGE_WIDTH		0x0100
#define IMAGE_LENGTH		0x0101
#define STRIP_OFFSET		0x0111
#define STRIP_BYTES_COUNTS	0x0117
#define COMPRESSION		0x0103
#define BITS_PER_SAMPLE		0x0102

#define HEADER_OFFSET		4
#define IFD_OFFSET		2
#define TAG_LEN			12
#define OFFSET_TO_DATA_TAG	8

// Function decode 8bpp grayscale tiff 
// and returns array of image data
int decode_tiff(char *filename, unsigned char **img_data, int *len, int *width);
